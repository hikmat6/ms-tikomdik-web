module.exports = {
  apps : [{
    name: "web-tikomdik-be",
    script: 'server.js',
    watch: '.',
    env: {
      "PORT": "1998",
      "SSO":"-",
      "URL_LOGIN_LOKAL" : "-",
      "URL_DB": "postgres://postgres:Tikomdik2019@103.155.104.11:5432/tikomdik_web",
      "JWT_CONF_TOKEN": "YXBpLXdlYi1vZmZpY2lhbC10aWtvbWRpay1rZXk=",
      "NODE_ENV":"development"
    }
  }],

  deploy : {
    production : {
      user : 'SSH_USERNAME',
      host : 'SSH_HOSTMACHINE',
      ref  : 'origin/master',
      repo : 'GIT_REPOSITORY',
      path : 'DESTINATION_PATH',
      'pre-deploy-local': '',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production',
      'pre-setup': ''
    }
  }
};
