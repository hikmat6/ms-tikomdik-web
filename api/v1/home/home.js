const express = require('express');
const router = express.Router();
//const validator = require('./jurusanValidator')
const Model = require('./../../model/homeModel');
const model = new Model();

router.get('/content', (req, res, next)=>{
    const idLang = req.query.is_lang;
    model.getContentHome(idLang).then(x=>{
        if(x != null ){
            res.send({
                status: 200,
                message: 'Success Get Content Home [Setting]',
                data:x
            })
        }else{
            res.send({
                status: 200,
                message: 'Success Get Content Home [Setting] but Null',
                data:{
                    url_video: "",
                    desk_education: "",
                    desk_technology: "",
                    desk_system: "",
                    url_image_education: "",
                    url_image_technology: "",
                    url_image_system: "",
                    name_about_us: "",
                    url_image_about_us: "",
                    desk_about_us: "",
                    url_image_side_slide: "",
                    created_date: ""
                }
            })
        }
    }).catch(err=>{
        console.log(err);
        res.status(500).send({
            message:"Error pada server"
        })
    }).catch(err=>{
        var details = {
            parent:err.parent,
            name:err.name,
            message:err.message
        }
        var error = new Error("Error pada server");
        error.status = 500;
        error.data = {
            date:new Date(),
            route:req.originalUrl,
            details:details
        };
        next(error);
    });
});

router.post('/homeNews', (req, res, next)=>{
    console.log('/homeNews')
    console.log(req.body)
    model.postDataHomeNews(req.body).then(x=>{
        res.send({
            data:x
        })
    }).catch( err => {
        console.log(err)
    })
});

router.get('/homeNews', (req, res, next)=>{
    console.log('/gethomeNews')
    model.getDataHomeNews().then( x => {         
        res.send(x)
   }).catch( err => {
       console.log(err)
   })
});
module.exports = router;