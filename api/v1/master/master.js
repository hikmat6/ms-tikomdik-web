const express = require('express');
const router = express.Router();
const Model = require('./../../model/masterModel');
const model = new Model();

router.get('/', (req, res, next)=>{
    model.getMasterAll().then( x => {         
          res.send({
              data:x,
          })
     }).catch( err => {
         console.log(err)
     })
});

router.get('/:id', (req, res, next)=>{
    model.getMasterOne(req.params).then( x => {         
          res.send({
              data:x,
          })
     }).catch( err => {
         console.log(err)
     })
});

router.post('/', (req, res, next)=>{
    console.log(req.body)
    model.postDataMaster(req.body).then(x=>{
        res.send({
            data:x
        })
    }).catch( err => {
        console.log(err)
    })
});

router.put('/', (req, res, next)=>{
    console.log(req.body)
    console.log("hit put link")
    model.putMaster(req.body).then(x=>{
        res.send({
            data:x
        })
    }).catch( err => {
        console.log(err)
    })
});

//delete
router.delete('/:id', (req, res, next) => {
    model.deleteMaster(req.params).then(x=>{
        res.send({
            data:x
        })
    }).catch( err => {
        console.log(err)
})
});

module.exports = router;

