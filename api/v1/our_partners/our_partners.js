const express = require('express');
const router = express.Router();
const Model = require('./../../model/ourPartnersModel');
const model = new Model();

router.get('/', (req, res, next)=>{
    model.getPartnersAll().then( x => {         
          res.send({
              data:x,
          })
     }).catch( err => {
         console.log(err)
     })
});

router.get('/:id', (req, res, next)=>{
    model.getPartnersOne(req.params).then( x => {         
            res.send({
                data:x,
            })
    }).catch( err => {
        console.log(err)
    })
});

router.post('/', (req, res, next)=>{
    model.postDataPartners(req.body).then(x=>{
        res.send({
            data:x
        })
    }).catch( err => {
        console.log(err)
    })
});

router.put('/', (req, res, next)=>{
    model.putPartners(req.body).then(x=>{
        res.send({
            data:x
        })
    }).catch( err => {
        console.log(err)
    })
});

//delete
router.delete('/:id', (req, res, next) => {
    model.deletePartners(req.params).then(x=>{
        res.send({
            data:x
        })
    }).catch( err => {
        console.log(err)
})
});
  
module.exports = router;