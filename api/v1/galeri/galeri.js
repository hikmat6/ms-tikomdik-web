const express = require('express');
const router = express.Router();
const Model = require('./../../model/galeriModel');
const model = new Model();

//get all
router.get('/', (req, res, next)=>{
    model.getGaleri().then( x => {         
          res.send({
              data:x,
          })
     }).catch( err => {
         console.log(err)
     })
});

//get by one
router.get('/:id', (req, res, next)=>{
    console.log(req.params)
    model.getGaleriOne(req.params).then( x => {         
            res.send({
                data:x,
            })
    }).catch( err => {
        console.log(err)
    })
});

//insert
router.post('/', (req, res, next)=>{
    if (req.file){
        req.body.url_image_galery = req.file.path;
    }
    model.postDataGaleri(req.body).then(x=>{
        res.send({
            data:x
        })
    }).catch( err => {
        console.log(err)
    })
});

//edit
router.put('/', (req, res, next)=>{
    model.putGaleri(req.body).then(x=>{
        res.send({
            data:x
        })
    }).catch( err => {
        console.log(err)
    })
});

//delete
router.delete('/:id', (req, res, next) => {
    model.deleteGaleri(req.params).then(x=>{
        res.send({
            data:x
        })
    }).catch( err => {
        console.log(err)
})
});

module.exports = router;