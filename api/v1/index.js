//Lib
const express = require('express');
const router = express.Router();
const routerHome = require('./home/home');
const routerMaster = require('./master/master');
const routerOurPartners = require('./our_partners/our_partners');
const routerGaleri = require('./galeri/galeri');
const routerNews = require('./news/news');
const routerProgram = require('./program/program');

router.use('/home', routerHome);
router.use('/our_partners', routerOurPartners);
router.use('/master', routerMaster);
router.use('/galeri', routerGaleri);
router.use('/news', routerNews);
router.use('/program', routerProgram);

module.exports = router;
