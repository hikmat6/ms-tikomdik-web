const express = require('express');
const router = express.Router();
const Model = require('./../../model/newsModel');
const model = new Model();
const fs = require('fs');
const path = require('path');

//get all
router.get('/', (req, res, next)=>{
    model.getNewsAll().then( x => {         
          res.send({
              data:x,
          })
     }).catch( err => {
         console.log(err)
     })
});

//get by one
router.get('/:id', (req, res, next)=>{
    model.getNewsOne(req.params).then( x => {         
            res.send({
                data:x,
            })
    }).catch( err => {
        console.log(err)
    })
});

//insert
router.post('/', (req, res, next)=>{
    if(!req.file){
        const err = new Error('Images harus di upload');
        err.errorStatus = 422;
        throw err;
    }
    const image = req.file.path;
    req.body.url_image_news = image;
    model.postDataNews(req.body).then(x=>{
        res.send({
            data:x
        })
    }).catch( err => {
        console.log(err)
    })
});

//insert
router.post('/addNews', (req, res, next)=>{
    console.log('add News')
    console.log(req.body)
    console.log(req.files)
    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).send('No files were uploaded.');
    }

    let targetFile = req.files.images;
    let extName = path.extname(targetFile.name);
    let baseName = path.basename(targetFile.name, extName);
    let uploadDir = path.join(__dirname, '../../../public/images', targetFile.name);

    let imgList = ['.png','.jpg','.jpeg','.gif'];
    // Checking the file type
    if(!imgList.includes(extName)){
        fs.unlinkSync(targetFile.tempFilePath);
        return res.status(422).send("Invalid Image");
    }

    if(targetFile.size > 1048576){
        fs.unlinkSync(targetFile.tempFilePath);
        return res.status(413).send("File is too Large");
    }

    let num = 1;
    while(fs.existsSync(uploadDir)){
        uploadDir = path.join(__dirname, '../../../public/images', baseName + '-' + num + extName);
        num++;
    }
    
    targetFile.mv(uploadDir, (err) => {
        if (err)
            return res.status(500).send(err);        
        
        req.body.url_image_news = baseName + ''+ extName;
        model.postDataNews(req.body).then(x=>{
            res.send({
                data:x
            })
        }).catch( err => {
            console.log(err)
        })
    });

    
});

//edit
router.put('/', (req, res, next)=>{
    model.putNews(req.body).then(x=>{
        res.send({
            data:x
        })
    }).catch( err => {
        console.log(err)
    })
});

//delete
router.delete('/:id', (req, res, next) => {
    model.deleteNews(req.params).then(x=>{
        res.send({
            data:x
        })
    }).catch( err => {
        console.log(err)
})
});


module.exports = router;