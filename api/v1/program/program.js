const express = require('express');
const router = express.Router();
const Model = require('./../../model/programModel');
const model = new Model();

//get all
router.get('/', (req, res, next)=>{
    model.getProgramAll().then( x => {         
            res.send({
                data:x,
            })
    }).catch( err => {
        console.log(err)
    })
});

//get by one
router.get('/:id', (req, res, next)=>{
    model.getProgramOne(req.params).then( x => {         
          res.send({
              data:x,
          })
     }).catch( err => {
         console.log(err)
     })
});

//insert
router.post('/', (req, res, next)=>{
    model.postDataProgram(req.body).then(x=>{
        res.send({
            data:x
        })
    }).catch( err => {
        console.log(err)
    })
});

//edit
router.put('/', (req, res, next)=>{
    model.putProgram(req.body).then(x=>{
        res.send({
            data:x
        })
    }).catch( err => {
        console.log(err)
    })
});

//delete
router.delete('/:id', (req, res, next) => {
    model.deleteProgram(req.params).then(x=>{
        res.send({
            data:x
        })
    }).catch( err => {
        console.log(err)
})
});

module.exports = router;