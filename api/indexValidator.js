const {query, body, validationResult, header, oneOf} = require('express-validator');
const {Op} = require('sequelize');
exports.validate = (method) =>{
    switch(method){        
        case 'verify_token':{
            return [
                header('authorization').notEmpty()
            ]
        }
        case 'verify_token_getUser':{
            return [
                header('Authorization').notEmpty()
            ]
        }
    }
}

exports.verify = (req, res, next) =>{
    try {
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            res.status(422).json({
                errors:errors.array()
            })
            return;
        }else{
            return next();
        }
    }catch(err){
      return next(err);
    }
}

