const {Op,Model, DataTypes, Sequelize,QueryTypes, DATE} = require('sequelize');
const sequelize = new Sequelize(process.env.URL_DB);
const { formatDate } = require("../../includes/dateHelper");

const getContentHome = sequelize.define('home', {
    id_content:{
        type:DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull:false,
        primaryKey:true
    },
    desk:{
        type:DataTypes.TEXT,        
        allowNull:true
    },
    is_lang:{
        type:DataTypes.CHAR,        
        allowNull:true
    },
    is_active:{
        type:DataTypes.CHAR,        
        allowNull:true
    },
    created_date:{
        type:DataTypes.DATE,        
        allowNull:true
    },
    last_update:{
        type:DataTypes.DATE,        
        allowNull:true
    },
    queue:{
        type:DataTypes.INTEGER,        
        allowNull:true
    },
    title:{
        type:DataTypes.CHAR,        
        allowNull:true
    },
    deskripsi:{
        type:DataTypes.TEXT,        
        allowNull:true
    },
},{
    schema:"public",
    tableName:"home",
    timestamps:false
})

class HomeModel {
    //GET
    getContentHome=(idLang)=>{
        return getContentHome.findOne({
            where:{
                is_lang: idLang,
                is_active: '1'
            }
          })
    }

    //insert
    postDataHomeNews(data){
        return getContentHome.create(data)
    }
    getDataHomeNews=()=>{
        return sequelize.query(`
            SELECT *
            FROM home`,{
            raw:false,
            type:QueryTypes.SELECT
        });
    }
}
module.exports = HomeModel;