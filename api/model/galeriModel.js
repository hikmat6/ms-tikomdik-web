const { seq } = require('async');
const {Op,Model, DataTypes, Sequelize,QueryTypes, DATE} = require('sequelize');
const sequelize = new Sequelize(process.env.URL_DB);
const { formatDate } = require("../../includes/dateHelper");

const TableGaleri = sequelize.define('galeri', {
    galeri_id :{
        type:DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull:false,
        primaryKey:true
    },
    title :{
        type:DataTypes.TEXT,
        allowNull:false
    },
    keterangan :{
        type:DataTypes.TEXT,
        allowNull:false
    },
    media :{
        type:DataTypes.TEXT,
        allowNull:false
    },
    url_thumb_galery :{
        type:DataTypes.TEXT,
        allowNull:false
    },
    url_image_galery :{
        type:DataTypes.TEXT,
        allowNull:false
    },
    created_date :{
        type:DataTypes.DATE,
        allowNull:false
    },
    last_update :{
        type:DataTypes.DATE,
        allowNull:false
    },
    updater_id :{
        type:DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull:false
    }
},{
    schema:"public",
    tableName:"galeri",
    timestamps:false
})

class galeriModel{
    //GET
    getGaleri=()=>{
        return sequelize.query(`
            SELECT *
            FROM galeri`,{
            raw:false,
            type:QueryTypes.SELECT
        });
    }

    //GET BY ONE
    getGaleriOne=(data)=>{  
        console.log(data.id)  
        return sequelize.query(`SELECT * FROM galeri WHERE galeri_id = :id`, {
            replacements:{
                id: data.id
            },
            raw:false,
            type:QueryTypes.SELECT
        })
    }
   
    //insert
    postDataGaleri(data){
        data.created_date = Date.now();
        data.last_update = Date.now();
        return TableGaleri.create(data)
    }

    //update
    putGaleri = (data) => {
        return TableGaleri.update({
            title:data.title,
            keterangan: data.keterangan,
            media: data.media,
            url_thumb_galery: data.url_thumb_galery,
            url_image_galery: data.url_image_galery,          
            last_update: Date.now()
        }, {
            where:{
                galeri_id:data.galeri_id
            }
        });
    }
    //DELETE
    deleteGaleri = (data) => {
        return sequelize.query("DELETE FROM galeri WHERE galeri_id = :id", {
            replacements:{
                id: data.id
            },
            type:QueryTypes.DELETE
        })
    }
}

module.exports = galeriModel;