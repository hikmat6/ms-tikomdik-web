const {Op,Model, DataTypes, Sequelize} = require('sequelize');
const sequelize = new Sequelize(process.env.URL_DB);

const tokenPayload = sequelize.define('ptk',{
    ptk_id:{
        allowNull:false,
        type:DataTypes.UUID,
        primaryKey:true
    },
    jabatan_ptk_id:{
        allowNull:false,
        type:DataTypes.STRING
    },
    npsn:{
        allowNull:false,
        type:DataTypes.STRING
    },
    nama_sekolah:DataTypes.STRING
},{
    sequelize,
    tableName:"v_jabatan_ptk",
    timestamps:false
});
const visitedWeb = sequelize.define('visited_user',{
    rand_uuid:{
        allowNull:false,
        type:DataTypes.UUID,
        primaryKey:true,
        defaultValue: DataTypes.UUIDV4
    },
    ip_user:{
        allowNull:false,
        type:DataTypes.STRING
    },
    browser_name:{
        allowNull:false,
        type:DataTypes.STRING
    },
    browser_version:{
        allowNull:false,
        type:DataTypes.STRING
    },
    user_os:{
        allowNull:false,
        type:DataTypes.STRING
    }
},{
    sequelize,
    tableName:"visited_user",
    timestamps:false,
    schema:'statistik'
});

//Lib
class indexModel{
    insertVisitedUser=(data)=>{
        return visitedWeb.create({
                ip_user: data.ip_user,
                browser_name: data.browser_name,
                browser_version: data.browser_version,
                user_os: data.user_os
            },{
            fields:['ip_user', 'browser_name', 'browser_version', 'user_os']
        })
    }
}
module.exports = indexModel;
