const { seq } = require('async');
const {Op,Model, DataTypes, Sequelize,QueryTypes, DATE} = require('sequelize');
const sequelize = new Sequelize(process.env.URL_DB);
const { formatDate } = require("../../includes/dateHelper");

const TableNews = sequelize.define('news', {
    id_news :{
        type:DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull:false,
        primaryKey:true
    },
    tag:{
        type:DataTypes.STRING,
        allowNull:true
    },
    sort_url:{
        type:DataTypes.TEXT,
        allowNull:true
    },
    title:{
        type:DataTypes.TEXT,
        allowNull:true
    },
    url_thumb_news:{
        type:DataTypes.TEXT,
        allowNull:true
    },
    deskripsi:{
        type:DataTypes.TEXT,
        allowNull:true
    },
    url_image_news:{
        type:DataTypes.TEXT,
        allowNull:true
    },
    overlay_theme:{
        type:DataTypes.STRING,
        allowNull:true
    },
    viewed:{
        type:DataTypes.INTEGER,
        allowNull:true
    },
    uploader:{
        type:DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull:true
    },
    is_lang:{
        type:DataTypes.CHAR,
        allowNull:true
    },
    queue:{
        type:DataTypes.INTEGER,
        allowNull:true
    },
    create_date:{
        type:DataTypes.DATE,
        allowNull:true
    },
    last_update:{
        type:DataTypes.DATE,
        allowNull:true
    },
    is_active:{
        type:DataTypes.CHAR,
        allowNull:true
    },
    id_generate:{
        type:DataTypes.INTEGER,
        allowNull:true
    }
},{
    schema:"public",
    tableName:"news",
    timestamps:false
})

class newsModel {
    //GET
    getNewsAll=()=>{
        return sequelize.query(`
            SELECT *
            FROM news`,{
            raw:false,
            type:QueryTypes.SELECT
        });
    }
    //GET BY ONE
    getNewsOne=()=>{
        return sequelize.query(`SELECT * FROM news WHERE id_news = :id`, {
            replacements:{
                id: data.id
            },
            raw:false,
            type:QueryTypes.SELECT
        })
    }
    //insert
    postDataNews(data){
        return TableNews.create(data)
    }
    //update
    putNews = (data) => {
        return TableNews.update({
            tag:data.tags,
            sort_url: data.sort_url,
            title: data.title,
            url_thumb_news: data.url_thumb_news,
            deskripsi: data.deskripsi,
            url_image_news: data.url_image_news,
            overlay_theme: data. overlay_theme,
            viewed: data.viewed,
            uploader: data.uploader,
            is_lang: data.is_lang,
            queue: data.queue,
            last_update: Date.now(),
            is_active: data.is_active
        }, {
            where:{
                id_news:data.id_news
            }
        });
    }
    //DELETE
    deleteNews = (data) => {
        return sequelize.query("DELETE FROM news WHERE id_news = :id", {
            replacements:{
                id: data.id
            },
            type:QueryTypes.DELETE
        })
    }
}

module.exports = newsModel;