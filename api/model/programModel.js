const { seq } = require('async');
const {Op,Model, DataTypes, Sequelize,QueryTypes, DATE} = require('sequelize');
const sequelize = new Sequelize(process.env.URL_DB);
const { formatDate } = require("../../includes/dateHelper");

const TabelProgram = sequelize.define('program',{
    id_program:{
        type:DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull:false,
        primaryKey:true
    },
    tag:{
        type:DataTypes.STRING,
        allowNull:false
    },
    sort_url:{
        type:DataTypes.STRING,
        allowNull:false
    },
    title:{
        type:DataTypes.STRING,
        allowNull:false
    },
    url_thumb_program:{
        type:DataTypes.TEXT,
        allowNull:true
    },
    konten:{
        type:DataTypes.TEXT,
        allowNull:true
    },
    url_image_program:{
        type:DataTypes.TEXT,
        allowNull:true
    },
    viewed:{
        type:DataTypes.TEXT,
        allowNull:true
    },
    is_lang:{
        type:DataTypes.CHAR,
        allowNull:false
    },
    updater_id:{
        type:DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull:false
    },
    create_date:{
        type:DataTypes.DATE,
        allowNull:false
    },
    last_update:{
        type:DataTypes.DATE,
        allowNull:false
    }
},{
    schema:"public",
    tableName:"program",
    timestamps:false
})

class programModel {
    //GET ALL
    getProgramAll=()=>{
        return sequelize.query(`
            SELECT *
            FROM program`,{
            raw:false,
            type:QueryTypes.SELECT
        });
    }

    //GET ONE
    getProgramOne=(data)=>{    
        return sequelize.query(`SELECT * FROM program WHERE id_program = :id`, {
            replacements:{
                id: data.id
            },
            raw:false,
            type:QueryTypes.SELECT
        })
    }

    //insert
    postDataProgram(data){
        return TabelProgram.create(data)
    }

    //update
    putProgram = (data) => {
        return TabelProgram.update({
            hak_akses:data.hak_akses,
            username:data.username            
        }, {
            where:{
                user_id:data.user_id
            }
        });
    }

    //DELETE
    deleteProgram = (data) => {
        return sequelize.query("DELETE FROM program WHERE user_id = :id", {
            replacements:{
                id: data.id
            },
            type:QueryTypes.DELETE
        })
    }

}
module.exports = programModel;