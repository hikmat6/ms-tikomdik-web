const {Op,Model, DataTypes, Sequelize,QueryTypes} = require('sequelize');
const sequelize = new Sequelize(process.env.URL_DB);
const { formatDate } = require("../../includes/dateHelper");

const getRefPangkat = sequelize.define('pangkat_golongan', {
    pangkat_golongan_id:{
        type:DataTypes.NUMBER,
        allowNull:false,
        primaryKey:true
    },
    kode:{
        type:DataTypes.STRING,
        allowNull:false
    },
    nama:{
        type:DataTypes.STRING,
        allowNull:false
    }
},{
    schema:"ref",
    tableName:"pangkat_golongan",
    timestamps:false
})

const getRefLembagaPengangkat = sequelize.define('lembaga_pengangkat', {
    lembaga_pengangkat_id:{
        type:DataTypes.NUMBER,
        allowNull:false,
        primaryKey:true
    },
    nama:{
        type:DataTypes.STRING,
        allowNull:false
    }
},{
    schema:"ref",
    tableName:"lembaga_pengangkat",
    timestamps:false
})

const getRefStatusKepegawaian = sequelize.define('status_kepegawaian', {
    status_kepegawaian_id:{
        type:DataTypes.INTEGER,
        allowNull:false,
        primaryKey:true
    },
    nama:{
        type:DataTypes.STRING,
        allowNull:false
    }
},{
    schema:"ref",
    tableName:"status_kepegawaian",
    timestamps:false
})

const getRefJenisPTK = sequelize.define('jenis_ptk', {
    jenis_ptk_id:{
        type:DataTypes.INTEGER,
        allowNull:false,
        primaryKey:true
    },
    jenis_ptk:{
        type:DataTypes.STRING,
        allowNull:false
    }
},{
    schema:"ref",
    tableName:"jenis_ptk",
    timestamps:false
})

const getRefAgama = sequelize.define('agama', {
    agama_id:{
        type:DataTypes.INTEGER,
        allowNull:false,
        primaryKey:true
    },
    nama:{
        type:DataTypes.STRING,
        allowNull:false
    }
},{
    schema:"ref",
    tableName:"agama",
    timestamps:false
})

const getRefPekerjaan = sequelize.define('pekerjaan', {
    pekerjaan_id:{
        type:DataTypes.INTEGER,
        allowNull:false,
        primaryKey:true
    },
    nama:{
        type:DataTypes.STRING,
        allowNull:false
    }
},{
    schema:"ref",
    tableName:"pekerjaan",
    timestamps:false
})

const getRefStatusPerkawinan = sequelize.define('status_perkawinan', {
    status_perkawinan_id:{
        type:DataTypes.INTEGER,
        allowNull:false,
        primaryKey:true
    },
    status_perkawinan:{
        type:DataTypes.STRING,
        allowNull:false
    }
},{
    schema:"ref",
    tableName:"status_perkawinan",
    timestamps:false
})

class RefModel {
    //GET
    getRefPangkat=()=>{
        return getRefPangkat.findAll()
    }

    getRefLembagaPengangkat=()=>{
        return getRefLembagaPengangkat.findAll()
    }

    getRefStatusKepegawaian=()=>{
        return getRefStatusKepegawaian.findAll()
    }

    getRefJenisPTK=()=>{
        return getRefJenisPTK.findAll()
    }

    getRefAgama=()=>{
        return getRefAgama.findAll()
    }

    getRefPekerjaan=()=>{
        return getRefPekerjaan.findAll()
    }

    getRefStatusPerkawinan = () => {
        return getRefStatusPerkawinan.findAll()
    }
}
module.exports = RefModel;