const { seq } = require('async');
const {Op,Model, DataTypes, Sequelize,QueryTypes, DATE} = require('sequelize');
const sequelize = new Sequelize(process.env.URL_DB);
const { formatDate } = require("../../includes/dateHelper");

const TabelMaster = sequelize.define('master',{
    user_id:{
        type:DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull:false,
        primaryKey:true
    },
    username : {
        type:DataTypes.TEXT,
        allowNull:false
    },
    hak_akses : {
        type:DataTypes.NUMBER,
        allowNull:false
    }
},{
    schema:"public",
    tableName:"master",
    timestamps:false
})

class masterModel {
    //GET ALL
    getMasterAll=()=>{
        return sequelize.query(`
            SELECT *
            FROM master`,{
            raw:false,
            type:QueryTypes.SELECT
        });
    }

    //GET ONE
    getMasterOne=(data)=>{    
        return sequelize.query(`SELECT * FROM master WHERE user_id = :id`, {
            replacements:{
                id: data.id
            },
            raw:false,
            type:QueryTypes.SELECT
        })
    }

    //insert
    postDataMaster(data){
        return TabelMaster.create(data)
    }

    //update
    putMaster = (data) => {
        return TabelMaster.update({
            hak_akses:data.hak_akses,
            username:data.username            
        }, {
            where:{
                user_id:data.user_id
            }
        });
    }

    //DELETE
    deleteMaster = (data) => {
        return sequelize.query("DELETE FROM master WHERE user_id = :id", {
            replacements:{
                id: data.id
            },
            type:QueryTypes.DELETE
        })
    }

}

module.exports = masterModel;