const { seq } = require('async');
const {Op,Model, DataTypes, Sequelize,QueryTypes, DATE} = require('sequelize');
const sequelize = new Sequelize(process.env.URL_DB);
const { formatDate } = require("../../includes/dateHelper");

const TableOurPartners = sequelize.define('our_partners', {
    id_partners:{
        type:DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull:false,
        primaryKey:true
    },
    tags:{
        type:DataTypes.STRING,
        allowNull:true
    },
    sort_url:{
        type:DataTypes.TEXT,
        allowNull:true
    },
    title:{
        type:DataTypes.TEXT,
        allowNull:true
    },
    url_thumb_partners:{
        type:DataTypes.TEXT,
        allowNull:true
    },
    konten:{
        type:DataTypes.TEXT,
        allowNull:true
    },
    url_image_partners:{
        type:DataTypes.TEXT,
        allowNull:true
    },
    viewed:{
        type:DataTypes.TEXT,
        allowNull:true
    },
    updater_id:{
        type:DataTypes.UUID,
        defaultValue: sequelize.fn('now'),
        allowNull:true
    },
    is_lang:{
        type:DataTypes.TEXT,
        allowNull:true
    },
    create_date:{
        type:DataTypes.DATE,
        allowNull:true
    },
    last_update:{
        type:DataTypes.DATE,
        defaultValue: sequelize.fn('now'),
        allowNull:true
    }
},{
    schema:"public",
    tableName:"our_partners",
    timestamps:false
})

class ourPartnersModel {
    //GET
    getPartnersAll=()=>{
        return sequelize.query(`
            SELECT *
            FROM our_partners`,{
            raw:false,
            type:QueryTypes.SELECT
        });
    }

    getPartnersOne=(data)=>{    
        return sequelize.query(`SELECT * FROM our_partners WHERE id_partners = :id`, {
            replacements:{
                id: data.id
            },
            raw:false,
            type:QueryTypes.SELECT
        })
    }

    //insert
    postDataPartners(data){
        return TableOurPartners.create(data)
    }

    //update
    putPartners = (data) => {
        return TableOurPartners.update({
            tags:data.tags,
            sort_url:data.sort_url,
            title:data.title,
            url_thumb_partners:data.url_thumb_partners,
            konten:data.konten,
            url_image_partners:data.url_image_partners,
            viewed:data.viewed,
            updater_id:data.updater_id,
            is_lang:data.is_lang,
            create_date:data.create_date,
            last_update:data.data.last_update
        }, {
            where:{
                id_partners:data.id_partners
            }
        });
    }

    //DELETE
    deletePartners = (data) => {
        return sequelize.query("DELETE FROM our_partners WHERE id_partners = :id", {
            replacements:{
                id: data.id
            },
            type:QueryTypes.DELETE
        })
    }
}
module.exports = ourPartnersModel;