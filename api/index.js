//Lib
const express = require('express');
const router = express.Router();
//const validator = require('./indexValidator');
const model = require('./model/indexModel');
//const axios = require('axios');
//const authChecker = require('./authChecker');
const routerV1 = require('./v1/index');
//const async = require('async');

router.post('/visited_web', (req, res, next) => {
    var body = req.body;
    new model().insertVisitedUser(body).then(x => {
        //console.log(x)
    }).catch(err => {
        var details = {
            parent:err.parent,
            name:err.name,
            message:err.message
        }
        var error = new Error("Error pada server");
        error.status = 500;
        error.data = {
            date:new Date(),
            route:req.originalUrl,
            details:details
        };
        next(error);
    })
})

// router.post('/getUserLogin', validator.validate('verify_token_getUser'), validator.verify, (req, res, next) => {
//     console.log(req.body);
//     var data = req.body;
//     if (data.user_type == "3") {
//         new model().getJabatanPayload({ id_pegawai: data.user_id }).then(x => {
//             if (Object.keys(x).length > 0) {
//                 var result = JSON.parse(JSON.stringify(x));
//                 data['id_jabatan'] = x.id_jabatan;
//                 res.send({
//                     id_jabatan: result.id_jabatan,
//                     nama_jabatan: result.nama_jabatan,
//                     nama: result.nama,
//                     token: authChecker.generateToken(data)
//                 });
//             } else {
//                 res.status(404).send({
//                     message: "User tidak ditemukan"
//                 })
//             }

//         }).catch(err => {

//         })
//     } else {
//         var nama_sekolah = "";
//         var jabatan = [];
//         async.waterfall([
//             (callback) => {
//                 new model().getTokenPayload({ 'ptk_id': data.user_id }).then(x => {
//                     if (Object.keys(x).length > 0) {
//                         data['npsn'] = x.npsn;
//                         nama_sekolah = x.nama_sekolah
//                         jabatan.push(x.jabatan_ptk_id);
//                     }
//                     callback(null);
//                 })
//             }, (callback) => {
//                 new model().getStatusWali({ 'ptk_id': data.user_id }).then(x => {

//                     if (x != null) {
//                         data['rombongan_belajar_id'] = x.rombongan_belajar_id;
//                         jabatan.push("wali_kelas");
//                     } else {

//                     }
//                     callback(null);
//                 })
//             }, (callback) => {
//                 if (jabatan.length < 1) {
//                     res.status(401).send({
//                         message: "User tidak terdaftar"
//                     });
//                 } else {
//                     data['jabatan'] = jabatan;
//                     res.send({
//                         jabatan: jabatan,
//                         nama_sekolah: nama_sekolah,
//                         token: authChecker.generateToken(data)
//                     });
//                 }

//             }
//         ]);
//     }
// })

router.use('/api/v1', (req, res, next) => {
    next();
}, routerV1);

module.exports = router;