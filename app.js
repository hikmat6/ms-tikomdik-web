//lib that loaded
const express = require('express');
const logging = require('morgan');
const fs = require('fs');
const dotenv = require('dotenv');
dotenv.config();
const bodyParser = require('body-parser');
const fileupload = require('express-fileupload');
connString = require('./includes/connString');
const Extras = require('./includes/extras');
extras = new Extras();
const winston = require('winston');
const app = express();
const path_main = require('path');


//Router Index
const routerIndex = require('./api/index');

/**Log system
 * log will sending to the file,
 * so admin can access directly to the file
 * temporary hardcoded for folder 
 */
const path = path_main.join(__dirname, "api/logapi");
const fileName = '/access.log';
try {
    if (!fs.existsSync(path)) {
        fs.mkdirSync(path);
    }
} catch (err) {
    console.error(err);
}


var writeFile = fs.createWriteStream(path + fileName, { flags: 'a' });
app.use(logging('combined', { stream: writeFile }));

//Body-Parser using for catching body parser (just in case needed)
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//express-fileupload using for file upload
app.use(fileupload({ useTempFiles: true, tempFileDir: './tmp/', createParentPath: false }));

/**CORS Avoidance.
 * Asterisk symbol(*) on Access-Control-Allow-Origin
 * should be replace with url for security issue.
 * Only GET, POST, PATCH, DELETE method for now,
 * can add with PUT or others for further.
 */
 app.use((req, res, next) => {
    const corsWhitelist = [
        'http://localhost:3000',
        'http://192.168.2.147:3000',
        'http://192.168.2.123:3000',
        'https://js3cis.tikomdik-disdikjabar.id'
    ];
    if (corsWhitelist.indexOf(req.headers.origin) !== -1) {
        res.header('Access-Control-Allow-Origin', req.headers.origin);
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    }
    
    if (req.method === 'OPTIONS') {
        res.header(
            'Access-Control-Allow-Methods', 'GET, POST,PUT, PATCH, DELETE'
        );
        return res.status(200).json({});
    }
    next();
});
const logger = winston.createLogger({
    format:winston.format.json(),
    transports:[
        new winston.transports.File({filename:path+"/error.log", level:"error"}),
        new winston.transports.File({filename:path+"/info.log", level:"info"}),
    ]
})

/**route which should handle
 * Add route in here
 */
app.use('/', routerIndex);
app.use('/images', express.static(path_main.join(__dirname, './public/images')));
//console.log( routerIndex )
app.use((req, res, next) => {
    var error = new Error('Not Found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    console.error("Error Route -> " + req.originalUrl);
    console.error(error.data);
    logger.error(error)
    res.status(error.status || 500);
    if(process.env.NODE_ENV != 'production'){
        res.json({
            message: error.message,
            detail: error.data
        });
    }else{
        res.json({
            message: error.message
        });
    }
});

//module export
module.exports = app;
