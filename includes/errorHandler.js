const logging = require('morgan');
const fs = require('fs');
const path = "./../api/logapi/";
try {
    if (!fs.existsSync(path)) {
        fs.mkdirSync(path);
    }
} catch (err) {
    console.error(err);
}

const express = require('express');
const app = express();
app.use((req, res)=>{
    res.status(500).send({
        message:"Eror"
    })
    logging("Coba", {stream:writeFile});
});
module.exports = app;