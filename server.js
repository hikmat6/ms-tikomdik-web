const http = require('http');
const cluster = require('cluster');
const cpu = require('os').cpus().length;
const app = require('./app');
const httpServer = http.createServer(app);

//Clustering process
if(process.env.MODE=="build"){
    httpServer.listen(3000);
    process.exit(0);
}else if(process.env.NODE_ENV!="production"){
    // io.on("connection", socket => {
    //     console.log('Socket: client connected');
    //     socket.on('disconnect', function () {
    //         console.log("Socket disconnected: " + socket.id);
    //        });
    // });
    console.log("Server is running on Port | " + process.env.PORT);
    httpServer.listen(process.env.PORT ||'dev'|| 3000);
}else{
    if (cluster.isMaster) {
        console.log('Master running process on pid: ' + process.pid);
        for (let i = 0; i < cpu; i++) {
            cluster.fork();
        }
    } else {
        //TesD
        const port = process.env.PORT || 3000;
        console.log('Worker running process on pids: ' + process.pid);
        httpServer.listen(port);
    }
}